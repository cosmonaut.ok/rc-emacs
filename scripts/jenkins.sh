#!/bin/bash

. config.in

JENKINSFILEMODE_URL="https://github.com/john2x/jenkinsfile-mode/archive/master.zip"
GROOVY_ELECTRIC_URL="https://raw.githubusercontent.com/Groovy-Emacs-Modes/groovy-emacs-modes/master/groovy-electric.el"
INF_GROOVY_URL="https://raw.githubusercontent.com/Groovy-Emacs-Modes/groovy-emacs-modes/master/inf-groovy.el"

function pkg_install
{
    get_url_with_name jenkinsfile.zip $JENKINSFILEMODE_URL
    get_url_with_name groovy-electric.el $GROOVY_ELECTRIC_URL
    get_url_with_name inf-groovy.el $INF_GROOVY_URL
    extract jenkinsfile.zip
    copy_to_local "jenkinsfile-mode-master/*" jenkinsfile
    copy_to_local groovy-electric.el groovy
    copy_to_local inf-groovy.el groovy
}


function pkg_update
{
    :
}

. include.in

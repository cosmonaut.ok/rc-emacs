#!/bin/bash

. config.in

DOCKERFILEMODE_URL="https://raw.githubusercontent.com/spotify/dockerfile-mode/master/dockerfile-mode.el"
DOCKER_COMPOSE_URL="https://github.com/meqif/docker-compose-mode/archive/master.zip"

function pkg_install
{
    get_url_with_name docker-compose-mode.zip $DOCKER_COMPOSE_URL
    extract docker-compose-mode.zip
    copy_to_local "docker-compose-mode-master/*" docker-compose-mode
}


function pkg_update
{
    :
}

. include.in

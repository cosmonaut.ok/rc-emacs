#!/bin/bash

. config.in

LATEX_TEMPLATES_URL="https://github.com/cosmonaut-ok/latex-templates/archive/master.zip"
LATEX_UNITS_URL="http://people.debian.org/~psg/elisp/latex-units.el"

function pkg_install
{
    get_url_with_name latex-templates.zip $LATEX_TEMPLATES_URL
    extract latex-templates.zip
    copy_to_local "latex-templates-master/*" latex-templates

    get_url_with_name latex-units.el $LATEX_UNITS_URL
    copy_to_local latex-units.el latex-units
}


function pkg_update
{
    :
}

. include.in

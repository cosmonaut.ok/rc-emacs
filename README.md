# rc-emacs
Configfiles for emacs installation

## General Conventions:

* General
```
S-TAB   - general completion
C-TAB   - special completion
C-M-TAB - yasnippet expansion
```
*  Dabbrev
```
M-/     - dabbrev-expand (
C-/     - company-dabbrev

* Yasnippet
```
C-c y   - yas-expand
C-c C-y - company-yasnippet
C-c i   - yas-insert-snippet

#!/bin/bash

. config.in

K8S_URL="https://raw.githubusercontent.com/TxGVNN/emacs-k8s-mode/master/k8s-mode.el"
KUBERNETES_URL="https://github.com/chrisbarrett/kubernetes-el/archive/refs/heads/master.zip"

function pkg_install
{
    get_url_with_name kubernetes-el.zip $KUBERNETES_URL
    extract kubernetes-el.zip
    copy_to_local "kubernetes-el-master/*" kubernetes-el
    
    get_url_with_name k8s-mode.el $K8S_URL
    copy_to_local k8s-mode.el k8s-mode
}


function pkg_update
{
    :
}

. include.in

(require 'terraform-mode)
(require 'company-terraform)

(eval-after-load "company"
  '(add-to-list 'company-backends 'company-terraform))

(defhooklet cosmonaut/terraform terraform-mode
  (company-terraform-init)
  (terraform-format-on-save-mode 1))

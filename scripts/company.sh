#!/bin/bash

. config.in

COMPANY_BOX_URL="https://github.com/sebastiencs/company-box/archive/master.zip"
FRAME_LOCAL_URL="https://raw.githubusercontent.com/sebastiencs/frame-local/master/frame-local.el"
function pkg_install
{
    get_url_with_name company-box.zip $COMPANY_BOX_URL
    extract company-box.zip
    copy_to_local "company-box-master/*" company-box

    get_url_with_name frame-local.el $FRAME_LOCAL_URL
    copy_to_local frame-local.el frame-local
}


function pkg_update
{
    :
}

. include.in

;;; bootstrap.el --- make Cosmonaut bootstrap and early boot  -*- lexical-binding: t -*-

;; Copyright (C) 2016 Alexander aka 'CosmonauT' Vynnyk

;; Maintainer: cosmonaut.ok@zoho.com
;; Keywords: internal
;; Package: cosmonaut

;; This file is part of Cosmonaut.

;; Cosmonaut is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; Cosmonaut is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Cosmonaut.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; TODO

;;; Code:

;; (defvar cosmonaut/source-directory (file-name-directory load-file-name))
(defvar cosmonaut/source-directory (file-name-directory load-file-name))
(defvar cosmonaut/packages-installed-p (expand-file-name "build" cosmonaut/source-directory))
(defvar cosmonaut/elget-user-recipes-path (concat cosmonaut/source-directory "el-get-user/recipes"))
(defvar cosmonaut/do-bootstrap t)
(defvar cosmonaut/el-get-force-reinit nil)

(setq user-emacs-directory cosmonaut/source-directory)

(defvar *package-get-packages-list*
  '(
    ;; libraries
    f
    s
    dash
    async
    ggtags
    let-alist
    apel
    cl-lib
    deferred
    ht
    clean-aindent-mode
    ;; helm
    helm
    helm-bibtex
    helm-company
    helm-dictionary
    helm-directory
    helm-flycheck
    helm-flyspell
    helm-git
    helm-ls-git
    helm-gtags
    ;; "helm-ipython"
    helm-ls-git
    helm-projectile
    helm-pydoc
    helm-robe
    helm-themes
    helm-swoop
    helm-slime
    ;; latex
    auctex
    cdlatex
    bibtex-utils
    latex-pretty-symbols
    latex-preview-pane
    magic-latex-buffer
    ;; python
    ;; python-mode
    ;; anaconda-mode
    elpy
    company-jedi
    ;; company
    company
    company-ansible
    company-auctex
    company-bibtex
    company-c-headers
    company-dict
    company-go
    company-inf-ruby
    ;; company-jedi
    company-math ;; for latex
    company-ngram
    company-quickhelp
    company-shell
    company-web
    company-terraform
    company-anaconda
    ;; web
    web-mode
    mmm-mode
    ;; docker
    dockerfile-mode
    ;; markdown
    markdown-changelog
    markdown-mode
    markdown-mode+
    markdown-preview-eww
    markdown-toc
    markdownfmt
    ;; yaml
    yaml-mode
    ;; json
    json
    json-mode
    json-reformat
    ;;
    js3-mode
    ;;
    cedet
    ecb
    sr-speedbar
    magit
    magit-popup
    gh ;; needed for magit-gh-pulls
    magit-gh-pulls
    fill-column-indicator
    highlight-parentheses
    highlight-symbol
    col-highlight
    flycheck
    flycheck-pos-tip
    ;; "flymake-ruby"
    tabbar
    yasnippet
    ;; yasnippets
    yasnippet-snippets ;; yasnippet/snippets are the same
    projectile
    ;; "ido-ubiquitous" ;; needed for magit
    ;; "ido-hacks"
    ;; "ido-vertical-mode"
    ;; "ido-better-flex"
    drag-stuff ;; move lines/regions up/down
    ssh
    terraform-mode
    terraform-doc
    groovy-mode
    ;; ;; lisp
    slime
    slime-company
    ;; slime-fuzzy
    ;; "slime-annot"
    redshank
    ;; c/c++
    use-package
    ;; notifications support
    notify
    ;; fun
    nyan-mode
    ;; themes
    ;; color-theme
    ;; solarized-emacs
    )
  )

(defvar *el-get-packages-list*
  '(
    ;; libraries and frameworks
    ;; "let-alist"
    ;; "apel"
    ;; "cl-lib"
    ;; "deferred"
    ;; "emacs-async"
    ;; "ht"
    ;; ruby
    ;; "ruby-mode"
    ;; "enh-ruby-mode"
    ;; "ruby-electric"
    ;; "ruby-hash-syntax"
    ;; "robe-mode"
    ;; "ruby-block"
    ;; "ruby-tools"
    ;; "ruby-refactor"
    ;; "rubocop"
    ;; "rvm"
    ;; "yari"
    ;; "bundler"
    ;; "rspec-mode" ;; moved to scripts
    ;; completion
    ;; "company-mode"
    ;; "company-inf-ruby"
    ;; "company-quickhelp"
    ;; "company-web"
    ;; c/c++
    ;; "ggtags"
    ;; "company-c-headers"
    ;; "clean-aindent"
    ;; docker/compose
    ;; "dockerfile-mode"
    ;; "docker-compose-mode"
    ;; markdown
    ;; "markdown-mode"
    ;; "markdown-preview-mode"
    ;; "markdown-toc"
    ;; yaml
    ;; "yaml-mode"
    ;; json
    ;; "json-mode"
    ;; "json-reformat"
    ;; tex
    ;; "auctex"
    ;; "reftex" TODO:
    ;; "cdlatex-mode"
    ;; "company-auctex"
    ;; "latex-preview-pane"
    ;; xml
    ;; web
    ;; "web-mode"
    ;; "web-completion-data"
    ;; "mmm-mode"
    ;; helm
    ;; "helm-company"
    ;; "helm-dictionary"
    ;; "helm-gtags"
    ;; "helm-ipython"
    ;; "helm-projectile"
    ;; "helm-pydoc"
    ;; "helm-robe"
    ;; "helm-shell"
    ;; "helm-shell-history"
    ;; "helm-themes"
    ;; "helm-swoop"
    ;; other
    ;; "ecb"
    ;; "sr-speedbar"
    ;; "magit"
    ;; "gh" ;; needed for magit-gh-pulls
    ;; ;; "magit-gh-pulls" moved to scripts
    ;; "fill-column-indicator"
    ;; "highlight-parentheses"
    ;; "highlight-symbol"
    ;; "col-highlight"
    ;; "flycheck"
    ;; "flycheck-pos-tip"
    ;; "flymake-ruby"
    ;; "tabbar"
    ;; "yasnippet"
    ;; "yasnippets"
    ;; ;; "yasnippet-snippets" ;; yasnippet/snippets are the same
    ;; "projectile"
    ;; "ido-ubiquitous" ;; needed for magit
    ;; "ido-hacks"
    ;; "ido-vertical-mode"
    ;; "ido-better-flex"
    ;; "drag-stuff" ;; move lines/regions up/down
    ;; "ssh"
    ;; "terraform-mode"
    ;; ;; lisp
    ;; "slime"
    ;; "slime-fuzzy"
    ;; "slime-company"
    ;; "slime-annot"
    ;; "redshank"
    ;; ;; c/c++
    ;; "use-package"
    ;; ;; notifications support
    ;; "notify"
    ;; ;; fun
    ;; "nyan-mode"
    ;; ;; themes
    ;; "color-theme"
    ;; "solarized-emacs"
    ))

(defun init-elget-user-recipes ()
  (message "init-elget-user-recipes")
  (unless (file-directory-p cosmonaut/elget-user-recipes-path)
    (mkdir cosmonaut/elget-user-recipes-path t))
  (when (file-exists-p "/tmp/el-get-master.zip")
    (delete-file "/tmp/el-get-master.zip"))
  (when (file-directory-p "/tmp/el-get-master")
    (delete-directory "/tmp/el-get-master" t))
  (let ((download-buffer (url-retrieve-synchronously "https://github.com/dimitri/el-get/archive/master.zip")))
    (save-excursion
      (set-buffer download-buffer)
      ;; we may have to trim the http response
      (goto-char (point-min))
      (re-search-forward "^$" nil 'move)
      (forward-char)
      (delete-region (point-min) (point))
      (write-file "/tmp/el-get-master.zip"))
    (shell-command "unzip -qq /tmp/el-get-master.zip -d /tmp/")
    (copy-directory "/tmp/el-get-master/recipes/" cosmonaut/elget-user-recipes-path nil t t)))

(defun init-el-get-packages ()
  "install el-get packages"
  (message "Installing el-get packages...")
  (el-get 'sync *el-get-packages-list*))

(defun el-get-install-packages ()
  "Install el-get"
  (message "Installing el-get...")
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.githubusercontent.com/dimitri/el-get/2.stable/el-get-install.el")
    (goto-char (point-max))
    (eval-print-last-sexp)))

;;; initialize package
(add-to-list 'load-path (concat cosmonaut/source-directory "el-get/el-get"))

;;; initialize el-get
(unless (require 'el-get nil 'noerror)
  (el-get-install-packages))

;; add user-defined recipes to el-get
(add-to-list 'el-get-recipe-path cosmonaut/elget-user-recipes-path)

;;; install package and el-get packages
(if (or (not (file-exists-p cosmonaut/packages-installed-p))
	cosmonaut/el-get-force-reinit)
    (progn
      ;; el-get initialization
      ;; (init-elget-user-recipes)
      (setq package-archives '(("elpa" . "http://tromey.com/elpa/")
                               ("gnu" . "http://elpa.gnu.org/packages/")
                               ("marmalade" . "http://marmalade-repo.org/packages/")
			       ("melpa" . "http://melpa.org/packages/")
			       ("org" . "http://orgmode.org/elpa/")))
      (package-initialize)
      (message "Getting elpa packages...")
      (package-refresh-contents)
      (dolist (i *package-get-packages-list*)
	(unless (package-installed-p i)
	  (package-install i)))
      (message "...Getting elpa packages [ DONE ]")
      (message "Getting el-get packages...")
      (init-el-get-packages)
      (message "...Getting el-get packages [ DONE ]")
      (write-region "" "" cosmonaut/packages-installed-p)
      ))

;;; Add 3rd party projects
(let ((default-directory (concat cosmonaut/source-directory "lib/")))
  (when (file-directory-p default-directory)
    (setq load-path
          (append
           (let ((load-path (copy-sequence load-path))) ;; Shadow
             (normal-top-level-add-subdirs-to-load-path))
           load-path))))

;;; Waiting for installation completed
(when (and (not (null (process-list))) cosmonaut/do-bootstrap)
  (message "Waiting while installation completed...")
  (sleep-for 30))
;; ;; awaiting
;; (loop
;;  t
;;  (if (null (process-list))
;;      (return)
;;    ;; (sleep-for 10)))
;;    (and  (sleep-for 10) (message "...still running. Waiting 10 sec..."))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; bootstrap.el ends here

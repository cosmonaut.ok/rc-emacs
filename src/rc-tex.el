;;; tex.el ---

;; Copyright (C) 2013 Alexander Vynnyk
;;
;; Author: cosmonaut@gmail.com
;; Keywords:
;; Requirements:
;; Status: not intended to be distributed yet

;;; hotkeys:
;;; TAB       : company-complete
;;; Shift+TAB : TeX-electric-macro
;;; Ctrl+TAB  : yasnippets
;;; F5        : TeX-command-run-all
;;; F8        : TeX-command-master
;;; C-c =     : table-of-contents of the article with navigation
;;; C-`       : insert math symbol from menu

;;; Code:
(defvar system-auctex-styles (locate-source-file "el-get/auctex/style/"))
(defvar local-auctex-styles (locate-source-file "style/"))
(defvar project-auctex-styles nil)

;; this strange code loads auctex
;; (autoload 'TeX-load-hack
;;   (expand-file-name "tex-site.el" (file-name-directory load-file-name)))
;; (TeX-load-hack)
(require 'tex-site (expand-file-name "tex-site.el"                                                                                                                                       
				     (file-name-directory load-file-name)))
	 
(require 'company-auctex)
(require 'latex-templates)
(require 'flycheck)

;; ;; C-c { Insert an environment template
;; (autoload 'cdlatex-mode "cdlatex" "CDLaTeX Mode" t)

;; (defhooklet cosmonaut/company-latex (tex-mode latex-mode LaTeX-mode) t
;;   (setq-local company-backends
;;               (append '(company-math-symbols-latex
;; 			company-backends)))

  ;; IF unicode really needed
  ;; (setq-local company-backends
  ;;             (append '((company-math-symbols-latex company-math-symbols-unicode))
  ;; 			company-backends)))

  ;; )

;;   ;; create auto lisp directory
;;   (dolist (v (append TeX-auto-private TeX-style-private))
;;     (when (not (file-directory-p v))
;;       (mkdir v t)))

;; default latex templates
(add-to-list 'latex-templates-private
             (locate-source-file "lib/latex-templates/templates/"))

;; add custom templates
(add-to-list 'latex-templates-private
             (locate-source-file "latex-templates/"))

(defhooklet cosmonaut/common-latex (tex-mode latex-mode LaTeX-mode) t
  (company-mode 1)
  (flycheck-mode 1)

  (autoload 'turn-on-cdlatex "cdlatex" "CDLaTeX Mode" nil)
  (cdlatex-mode 1)
  
  ;;   (outline-minor-mode 1)
  ;;   (yas-minor-mode 1)
  (company-auctex-init)
  ;;   ;; (magic-latex-buffer 1)
  ;;   ;; (latex-preview-pane-mode 1)

  (flyspell-mode 1)
  ;;   (flyspell-buffer)
  ;;   ;; (turn-on-bib-cite)
  ;;   ;; (setq bib-cite-use-reftex-view-crossref t)

  (custom-set-variables
   '(project-auctex-styles (concat (file-name-directory (buffer-file-name)) "style/"))
   ;; autoinsert math block closing
   '(TeX-electric-math (cons "$ " " $"))
   ;; use "english" quotes
   '(TeX-open-quote "``")
   '(TeX-close-quote "''")
   '(preview-auto-cache-preamble nil)
   '(preview-scale-function 1.3)
   '(TeX-auto-save nil)
   '(TeX-parse-self t)
   ;; '(TeX-save-query nil)
   '(TeX-PDF-mode t)
   ;; '(TeX-master (locate-source-file "share/master"))
   '(TeX-default-mode (quote Latex-mode))

   ;;    ;; hooks list, with new section
   ;;    ;; '(LaTeX-section-hook '(LaTeX-section-heading LaTeX-section-title LaTeX-section-section LaTeX-section-label))
   '(LaTeX-section-hook
     '(LaTeX-section-heading
       LaTeX-section-title
       LaTeX-section-toc
       LaTeX-section-section
       LaTeX-section-label))
   ;;    ;; '(TeX-arg-item-label-p nil)    ; set t, if you want to be asked for item label every time with C-c C-j
   ;;    ;; ;; (LaTeX-eqnarray-label "eq")
   ;;    ;; ;; (LaTeX-equation-label "eq")
   ;;    ;; ;; (LaTeX-figure-label "fig")
   ;;    ;; ;; (LaTeX-table-label "tab")
   '(TeX-newline-function 'reindent-then-newline-and-indent)
   '(TeX-style-path
     (list
      system-auctex-styles
      local-auctex-styles
      project-auctex-styles))
   ;;    '(outline-minor-mode-prefix "\C-c \C-o") ; Or something else
   )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;; USING reftex-mode: https://www.gnu.org/software/auctex/manual/reftex/RefTeX-in-a-Nutshell.html#RefTeX-in-a-Nutshell
(defhooklet cosmonaut/reftex-hook (tex-mode latex-mode LaTeX-mode) t
  (autoload 'reftex-mode "reftex" "RefTeX Minor Mode" t)
  (setq reftex-plug-into-AUCTeX t)
  (autoload 'turn-on-reftex "reftex" "RefTeX Minor Mode" nil)
  ;; ;; (autoload 'reftex-citation "reftex-cite" "Make citation" nil)
  ;; ;; (autoload 'reftex-index-phrase-mode "reftex-index" "Phrase Mode" t)
  (autoload 'turn-on-bib-cite "bib-cite")
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defhooklet cosmonaut/texinfo-hook texinfo-mode t
  (local-set-key [delete]  'delete-char)
  (define-key texinfo-mode-map [f10] 'tex-math-preview)
  (setq delete-key-deletes-forward t))

(defun flymake-get-tex-args (file-name)
  (list "pdflatex"
        (list "-file-line-error" "-draftmode" "-interaction=nonstopmode" file-name)))

;; (setq ispell-program-name "aspell") ; could be ispell as well, depending on your preferences
;; (setq ispell-dictionary "english") ; this can obviously be set to any language your spell-checking program supports

(defhooklet cosmonaut/keymap-latex (tex-mode latex-mode LaTeX-mode) t
  (local-set-key (kbd "<tab>") 'company-complete)
  (local-set-key (kbd "<backtab>") 'TeX-electric-macro)
  (local-set-key (kbd "<C-tab>") 'yas-expand)
  ;; (local-set-key "<tab>" 'cdlatex-tab)

  ;; CDLATEX-related tweaks
  (local-set-key (kbd "C-c C-e") 'cdlatex-environment)
  (local-set-key (kbd "C-`") 'cdlatex-math-symbol)
  (local-unset-key (kbd "`"))

  (local-set-key (kbd "<f5>") 'TeX-command-run-all)
  (local-set-key (kbd "<f8>") 'TeX-command-master)
  )


(add-to-list 'auto-mode-alist '("\\.tex" . LaTeX-mode))

(require 'groovy-mode)
(require 'jenkinsfile-mode)
(require 'inf-groovy)

; accept also single quote as double quote for strings
(add-hook 'groovy-mode-hook
          (lambda () (modify-syntax-table ?\' "\"")))

(add-auto-mode 'jenkinsfile-mode "Jenkinsfile" "\\.jdp")

(custom-set-variables '(groovy-indent-offset 2))

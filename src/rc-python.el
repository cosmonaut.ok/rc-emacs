;;; python.el --- ruby language support  -*- lexical-binding: t -*-

;; Copyright (C) 2016 Alexander aka 'CosmonauT' Vynnyk

;; Maintainer: cosmonaut.ok@zoho.com
;; Keywords: internal
;; Package: cosmonaut

;; This file is part of Cosmonaut.

;; Cosmonaut is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; Cosmonaut is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Cosmonaut.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; TODO:

;;; Code:

;;; Package --- Cosmonaut

;;; Commentary:

;;; Code:

;;;
;;; Generic
;;;

;; (require 'python-mode)
;; (require 'anaconda-mode)

;; (eval-after-load "company"
;;   '(add-to-list 'company-backends 'company-jedi))
 ;; '(add-to-list 'company-backends '(company-anaconda :with company-capf)))

;; (setq jedi:server-args `("--log-level=DEBUG"
;;                          ,(format "--log=%s" (expand-file-name "~/jediepcserver.log"))
;;                          "--log-traceback"))

(defhooklet cosmonaut/python-generic python-mode t
  (jedi:setup)
  ;; enable semantic mode
  ;; (semantic-mode 1)
  ;; set keys
  (local-set-key (kbd "<backtab>") 'company-complete)
  (local-set-key (kbd "<C-tab>") 'company-jedi)

  (semantic-mode 1)
  )

;;; rc-python.el ends here
